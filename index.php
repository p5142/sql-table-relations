<?php require "connection.php";
ob_start();?>
<?php require "config.php"?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>TASKS ASSIGNED</title>
</head>
<body>
<h1>TASKS ASSIGNED</h1>
<form method="post">
<table>
    <!-- COLUMN TITLES -->
    <tr>
        <th>TASK</th>
        <th>PERSON ASSIGNED</th>
    </tr>
    <!-- DISPLAY DATA INDIVIDUALLY -->
    <?php 
    // START IF
    if(!empty($result9) === TRUE) {
    ?>
        <?php 
        // START LOOP
            forEach($result9 as $res9){
            ?>
            <!-- ROW ELEMENTS -->
                <tr>
                    <td><?php echo $res9['task']?></td>
                    <td><?php echo $res9['person'] ?></td>
                    <td><a href="./modify.php?id=<?= $res9['id'] ?>">Modify</a></td>
                    <td><button type="submit" name="delete" value= "<?= $res9['id']?>">DELETE</button></td>
                </tr>
        <!-- END LOOP -->
        <?php
        }
    // END IF
    }
    ?>
</table>
</form>
<a href="assign_task_final.php"><button>ASSIGN A TASK</button></a>
<a href="task.php"><button>TASKS TABLE</button></a>
<a href="people.php"><button>PEOPLE TABLE</button></a>
</body>
</html>
